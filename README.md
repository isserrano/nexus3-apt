# nexus3-apt

Nexus3 with the apt plugin installed

1- Clone the repository

2- `cd nexus3-apt`

3- Build the container: `docker build -t nexus3-apt .`

4- Start nexus3: `docker run -ti -d -p 8081:8081 --name nexus3 nexus3-apt`

